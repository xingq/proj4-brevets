"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

ACP_ERROR_CONTROL_MORE_THAN_BREVET = 1
ACP_ERROR_INVALID_DISTANCE = -1
ACP_ERROR_INVALID_DATE = -2


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
        control_dist_km:  number, the control distance in kilometers
        brevet_dist_km: number, the nominal distance of the brevet
            in kilometers, which must be one of 200, 300, 400, 600,
            or 1000 (the only official ACP brevet distances)
        brevet_start_time:  An ISO 8601 format date-time string indicating
            the official start time of the brevet
    Returns:
        An ISO 8601 format date string indicating the control open time.
        This will be in the same time zone as the brevet start time.
    """

    try:
        control_dist_km = int(control_dist_km)
        brevet_dist_km = int(brevet_dist_km)
    except:
        return ACP_ERROR_INVALID_DISTANCE
    
    if control_dist_km < 0 or brevet_dist_km < 0:
        return ACP_ERROR_INVALID_DISTANCE
    
    if control_dist_km > brevet_dist_km:
        return ACP_ERROR_CONTROL_MORE_THAN_BREVET

    try:
        input_time = arrow.get(brevet_start_time)
    except:
        return ACP_ERROR_INVALID_DATE

    max_speed_list = [0, 200, 400, 600, 1000]
    min_speed_dict = {
        max_speed_list[0]: 34,
        max_speed_list[1]: 32,
        max_speed_list[2]: 30,
        max_speed_list[3]: 28,
        max_speed_list[4]: 26
    }
    hours = 0.00
    current_control_dist_km = control_dist_km
    max_speed_list.sort(reverse=True)
    for max_speed in max_speed_list:
        if current_control_dist_km > max_speed:
            hours += (current_control_dist_km - max_speed) / min_speed_dict[max_speed]
            current_control_dist_km = max_speed

    input_timestamp = input_time.timestamp
    result_timestamp = input_timestamp + round(hours*60, 0)*60

    return arrow.get(result_timestamp).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    try:
        control_dist_km = int(control_dist_km)
        brevet_dist_km = int(brevet_dist_km)
    except:
        return ACP_ERROR_INVALID_DISTANCE
    
    if control_dist_km < 0 or brevet_dist_km < 0:
        return ACP_ERROR_INVALID_DISTANCE
    
    if control_dist_km > brevet_dist_km:
        return ACP_ERROR_CONTROL_MORE_THAN_BREVET

    try:
        input_time = arrow.get(brevet_start_time)
    except:
        return ACP_ERROR_INVALID_DATE

    max_speed_list = [0, 600, 1000]
    min_speed_dict = {
        max_speed_list[0]: 15,
        max_speed_list[1]: 11.428,
        max_speed_list[2]: 13.333
    }
    hours = 0.00
    current_control_dist_km = control_dist_km
    max_speed_list.sort(reverse=True)
    for max_speed in max_speed_list:
        if current_control_dist_km > max_speed:
            hours += (current_control_dist_km - max_speed) / min_speed_dict[max_speed]
            current_control_dist_km = max_speed

    # specific just for 200km, it just so confusing !!!!!!
    if control_dist_km == 200:
        hours = 13.5

    input_timestamp = input_time.timestamp
    result_timestamp = input_timestamp + round(hours*60, 0)*60

    return arrow.get(result_timestamp).isoformat()
