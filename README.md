# README # 

## Author: Xing Qian, xingq@uoregon.edu ##

This is a solution of the assignment "project4" project for CIS 322

You can view the project description: https://bitbucket.org/UOCIS322/proj4-brevets/src/master/

## Introduction of my solution

The assignment asked for build the brevets program by jQuery Ajax, and I have reached this goal.

## Run the project

```
# go to the brevets folder, and run the following command build image.
docker build -t proj4-brevets:latest .

# run image
docker run -d -p 5000:5000 proj4-brevets

# tests by nosetests
nosetests
```

## Details for this assignment

- `open_time` and `close_time` methods of `acp_times.py` would return following values:
    - **date string**: All inputs valid.
    - **1**: When input control distance greater than brevets distance.
    - **-1**: When input control distance or brevets distance are not positive number.
    - **-2**: When input start date is invalid date.
- If the brevets distance or the begin date or time changed, there would invoke the update of all the close and open time.
- If input error, the error message would be displayed.
- When the control distance is `200km`, the close time would be calculated as 13h30 instead of 13h20.