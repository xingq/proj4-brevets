"""
Nose tests for acp_times.py
"""
from acp_times import open_time, close_time

import arrow
import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

def test_location_100():
    control_dist_km = 100
    brevet_dist_km = 200
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_open_time = arrow.get('2020-11-01 02:56:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_close_time = arrow.get('2020-11-01 06:40:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_open_time
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_close_time

def test_location_200():
    control_dist_km = 200
    brevet_dist_km = 200
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_open_time = arrow.get('2020-11-01 05:53:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_close_time = arrow.get('2020-11-01 13:30:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_open_time
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_close_time

def test_location_300():
    control_dist_km = 250
    brevet_dist_km = 300
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_open_time = arrow.get('2020-11-01 07:27:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_close_time = arrow.get('2020-11-01 16:40:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_open_time
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_close_time

def test_location_400():
    control_dist_km = 400
    brevet_dist_km = 400
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_open_time = arrow.get('2020-11-01 12:08:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_close_time = arrow.get('2020-11-02 02:40:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_open_time
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_close_time

def test_location_600():
    control_dist_km = 600
    brevet_dist_km = 600
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_open_time = arrow.get('2020-11-01 18:48:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_close_time = arrow.get('2020-11-02 16:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_open_time
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_close_time

def test_location_1000():
    control_dist_km = 1000
    brevet_dist_km = 1000
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_open_time = arrow.get('2020-11-02 09:05:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_close_time = arrow.get('2020-11-04 03:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_open_time
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_close_time

def test_location_over_1000():
    control_dist_km = 1100
    brevet_dist_km = 1300
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_open_time = arrow.get('2020-11-02 12:56:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    expect_close_time = arrow.get('2020-11-04 10:30:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_open_time
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == expect_close_time

def test_location_control_more_than_brevet():
    control_dist_km = 1100
    brevet_dist_km = 200
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == 1
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == 1

def test_location_minus_input():
    control_dist_km = -100
    brevet_dist_km = 100
    brevet_start_time = arrow.get('2020-11-01 00:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == -1
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == -1

def test_location_error_date():
    control_dist_km = 100
    brevet_dist_km = 200
    brevet_start_time = 'abcd'
    assert open_time(control_dist_km, brevet_dist_km, brevet_start_time) == -2
    assert close_time(control_dist_km, brevet_dist_km, brevet_start_time) == -2
